import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BinaryHeapTest {

    @Test
    public void verifyThatHeapCanKeepElements() {
        // given
        BinaryHeap heap = new BinaryHeap();
        heap.add(123);
        //when
        int actual = heap.removeMax();
        //then
        Assert.assertEquals(123, actual);
    }

    @Test
    public void verifyThatHeapAlwaysReturnMax() {
        BinaryHeap heap = new BinaryHeap();
        heap.add(333);
        heap.add(5);
        heap.add(123);



        assertEquals(333, heap.removeMax());
        assertEquals(123, heap.removeMax());
        assertEquals(5, heap.removeMax());
    }

    @Test
    public void verifiyThatHeapCanContainHundredElements(){
        BinaryHeap heap = new BinaryHeap();
        for(int i = 0; i < 100; i++){
            heap.add(i);
        }
        for(int i = 99; i > 0; i--) {
            assertEquals(i, heap.removeMax());
        }
    }

}

