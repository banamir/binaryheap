import java.util.ArrayList;

public class BinaryHeap {
    ArrayList<Integer> values = new ArrayList();
    int size = 0;


    public void add(int element) {
        values.add(element);// додаємо в кінець
        size++;
        swim(size-1);// піднімаємо вгору

    }

    public int removeMax() {
        int result = values.get(0); // повертаємо перший
        values.set(0, values.get(--size));// вставляємо останній
        values.remove(size);
        sink(0); // топимо вниз
        return result;

    }


    private void swim(int index) {
        while (values.get(index/2) < values.get(index)) {
            swap(index/2, index);
            index = index/2;
        }
    }

    public void swap(int child, int parent) {
        int tmp = values.get(child);
        values.set(child, values.get(parent));
        values.set(parent, tmp);
    }

    private void sink(int index) {
        while(2*index < size){
            int child = 2*index; //індекс лівого нащадка

            if(child < size - 1 && values.get(child) < values.get(child+1)){
                child++; // беремо правого нащадка, якщо правий більший за лівого
            }
            if(values.get(index) >= values.get(child)) {
                break; // якщо батько менший більший за нащадка, кінець спуску
            }
            swap(index, child);
            index = child;
        }
    }
}

